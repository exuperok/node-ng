#!/bin/sh
sudo apt-get -y update
sudo apt-get -y upgrade

#Install external dependencies: git, curl, python dependencies etc...
sudo apt-get -y install git imagemagick curl build-essential tcl8.5 re2c openssl libssl0.9.8
sudo apt-get -y install openjdk-7-jre  python-software-properties python g++ make pkg-config libpq-dev

#install webserver ngynx
sudo apt-get -y install nginx

#Install python packages
sudo apt-get install -y python python-dev python-setuptools build-essential 


#Install ruby packages
sudo \curl -L https://get.rvm.io | bash -s stable
sudo source ~/.rvm/scripts/rvm
rvm requirements
rvm install ruby
rvm use ruby --default
rvm rubygems current
gem install rails


#Install OpenJDK7
sudo apt-get -y install openjdk-7-jre
if [ ! -f /usr/java/default ];
then
    sudo mkdir /usr/java
    sudo ln -s /usr/lib/jvm/java-7-openjdk-amd64 /usr/java/default
fi


#Add  nodejs, elasticsearch repositories
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo wget -O - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | apt-key add -
sudo add-apt-repository -y 'deb http://packages.elasticsearch.org/elasticsearch/1.2/debian stable main'
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
sudo echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
if [ ! -f rabbitmq-signing-key-public.asc ];
then
    sudo wget -q http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
fi
sudo apt-key add rabbitmq-signing-key-public.asc
sudo add-apt-repository -y 'deb http://www.rabbitmq.com/debian/ testing main'
sudo apt-get update


#Install Nodejs & geric dependencies for testing, quality analysis and code coverage
sudo apt-get -y install nodejs
npm install -g --save-dev grunt-cli #tasks runner
npm install -g --save-dev bower #dependecny management
npm install -g --save-dev chai #TDD & BDD assertion library
npm install -g --save-dev qunitjs #TDD and BDD Framework
npm install -g --save-dev jasmine-node #TDD and BDD Framework
npm install -g --save-dev karma --save-dev #E2E testing in JS
npm install -g --save-dev karma-jasmine karma-chrome-launcher --save-dev #E2E testing in JS in the browser
npm install -g --save-dev nightwatch #E2E testing in JS
npm install -g --save-dev istanbul #code coverage for js framework
npm install -g --save-dev jshint # detect errors and potential problems in JavaScript code
npm install -g --save-dev jscs #detect copy paste pattern in js code
npm install -g --save-dev escomplex #detect high level of cyclomatic complexity in js code
npm install -g --save-dev yo #scafolding project generator for js
npm install -g --save-dev plato #source code visualization, static analysis, and complexity tool
#Installing selenium webtest runner
if [ ! -f /usr/local/bin/selenium-server/ ];
then
    wget http://selenium-release.storage.googleapis.com/2.42/selenium-server-standalone-2.42.0.jar
    sudo mv selenium-server-standalone-2.42.0.jar /opt/
    sudo ln -s /opt/selenium-server-standalone-2.42.0.jar /usr/local/bin/selenium-server
fi

#Installing headless browser for testing purposes
npm install -g --save-dev phantomjs
npm install -g --save-dev casperjs

#install mongodb
sudo apt-get -y install mongodb-org

#Install Elastic Search
sudo apt-get -y install elasticsearch

#Install RabbitMQ
if [ ! -f /var/log/rabbitmqsetup ];
then
    sudo sudo apt-get -y install rabbitmq-server
    sudo service rabbitmq-server start
    sudo rabbitmq-plugins enable rabbitmq_management
    sudo rabbitmqctl add_user admin password
    sudo rabbitmqctl set_user_tags admin administrator
    sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
    sudo rabbitmqctl delete_user guest
    sudo service rabbitmq-server restart

    sudo touch /var/log/rabbitmqsetup
fi

#Create Mariadb root profile
echo mariadb-server-5.5 mysql-server/root_password password root | sudo debconf-set-selections
echo mariadb-server-5.5 mysql-server/root_password_again password root | sudo debconf-set-selections


#install mariadb and php extension
sudo apt-get install -y mariadb-server

# Setup roject database
# database name, username & password need to be changed per project
if [ ! -f /var/log/databasesetup ];
then
    echo "DROP DATABASE IF EXISTS lampdb" | mysql -uroot -proot
    echo "CREATE USER 'lampdb'@'localhost' IDENTIFIED BY 'lampdb'" | mysql -uroot -proot
    echo "CREATE DATABASE lampdb" | mysql -uroot -proot
    echo "GRANT ALL ON lampdb TO 'lampdb'@'localhost'" | mysql -uroot -proot
    echo "flush privileges" | mysql -uroot -proot

    sudo touch /var/log/databasesetup
fi
