#node-ng
Simple Vagrant box to bootstrap a dev environnement with nodejs, nginx, mariadb and mongodb


## Features
- This shell script provisions a vagrant box by installing packages
- VM Description
	- 1GB RAM
	- Ubuntu 12.04 LTS 64-bit
	- Nginx
	- Mariadb
	- MongoDB
	- Nodejs
	- Elastic search
	- RabbitMQ
	- Git


## Setup
The `Vagrantfile` shares 3 folders from your host machine into your VM: '/vagrant/data, "/vagrant/scripts".  If these folders don't exist they will be created when `vagrant up` is run. 

These folders are used as follows:
- vagrant
	- This is the root of the VM and the Project
- vagrant/data
	- This is where the data file are stored. 
	- datadump files will be created there.
- vagrant/logs
	- This is where application logs will be dumpped
- vagrant/script
	- This is where system wide script will be stored

## What is the provision.sh script doing?
- Updates software on the VM
- Installs necessary packages
- Creates the `devdb` Mariadb database and user
- Imports `database.sql` into the `devdb` database if the file exists
- Restarts nginx

## Creating the database dump file
This  command, run from inside the VM, will dump the `devdb` database into a file called `database.sql` and place it into `/vagrant/data/` which should be shared with host computer, thus it will continue to exist even if the VM is destroyed.

`mysqldump -uroot -proot devdb > /vagrant/data/`

##start using the vm
Edit your local hosts file to point a domain to `192.168.56.103` then use url in a browser to hit the application your VM is serving.
